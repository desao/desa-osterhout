﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace Lab_3
{
    class BaseballPlayer : IComparable<BaseballPlayer>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int YearsPlayed { get; set; }
        public double Salary { get; set; }
        public string PlayerId { get; set; }

        public BaseballPlayer(string firstName, string lastName, double salary, string playerId)
        {
            FirstName = firstName;
            LastName = lastName;
            Salary = salary;
            PlayerId = playerId;
            YearsPlayed = 1;
        }

        public void AddSalary (double salary)
        {
            Salary += salary;
            YearsPlayed++;
        }

        public double AvgSalary
        {
            get { return Salary / YearsPlayed; }
        }
        public int CompareTo([AllowNull] BaseballPlayer other)
        {
            return AvgSalary.CompareTo(other.AvgSalary);
        }

        public override string ToString()
        {
            return "Name: " + FirstName + " " + LastName + " Salary: " + Salary; 
        }
    }
}
