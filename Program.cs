﻿using System;
using System.Collections.Generic;

namespace Lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            PlayerReader util = new PlayerReader();
            
            Dictionary<string, BaseballPlayer> players =  util.ReadCsvFile();

            List<BaseballPlayer> baseballPlayers = new List<BaseballPlayer>();
            baseballPlayers.AddRange(players.Values);
            baseballPlayers.Sort();
            
            foreach (BaseballPlayer baseballPlayer in baseballPlayers)
            {
                Console.WriteLine(baseballPlayer);
            }

           
        }
    }
}
