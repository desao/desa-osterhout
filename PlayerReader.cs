﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;

namespace Lab_3
{
    class PlayerReader
    {

        public Dictionary<string, BaseballPlayer> ReadCsvFile()
        {
            Dictionary<string, BaseballPlayer> players = new Dictionary<string, BaseballPlayer>();

            using (var reader = new StreamReader("Salary.csv"))
            {
                reader.ReadLine();
                while (!reader.EndOfStream)
                {
                    String line = reader.ReadLine();

                    String[] input = line.Split(new char[] { ',' });
                    String PlayerId = input[7];

                    if (players.ContainsKey(PlayerId))
                    {
                        BaseballPlayer baseballPlayer = players[PlayerId];
                        baseballPlayer.AddSalary(Convert.ToDouble(input[6]));
                    }
                    else
                    {
                        BaseballPlayer baseballPlayer = new BaseballPlayer(input[2], input[1], Convert.ToDouble(input[6]), input[7]);
                        players.Add(baseballPlayer.PlayerId, baseballPlayer);


                    }

                    
                }

                return players;
            }
        }

    }
}
